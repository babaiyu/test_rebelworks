/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  poweredByHeader: false,
  env: {
    API_KEY: 'cf351551b4b72640578e20445eebffd6',
    URL: 'https://api.themoviedb.org/3',
    IMG_URL: 'https://image.tmdb.org/t/p/w780',
  },
  images: {
    domains: ['image.tmdb.org'],
  },
};

module.exports = nextConfig;

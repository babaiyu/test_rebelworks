import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
import {Carousel} from 'react-responsive-carousel';
import Image from 'next/image';
import {useMemo} from 'react';

const IMG_URL = process.env.IMG_URL;

interface Props {
  data: any[];
  onClick: (id: any) => void;
}

export default function MyCarousel({data, onClick}: Props) {
  const renderList = useMemo(() => {
    return data.map(item => {
      const _onClick = () => {
        onClick(item?.id);
      };

      return (
        <div key={item?.id}>
          <div className="absolute z-10 flex flex-col items-start justify-between md:h-screen h-72 w-full p-10 bg-gradient-to-r from-gray-900">
            <figure className="object-cover md:h-10 md:w-10 h-7 w-7 relative mb-3 flex-initial">
              <Image
                src="/assets/logo.png"
                alt="Icon Logo"
                loading="lazy"
                layout="fill"
              />
            </figure>
            <div className="md:w-1/2 w-full">
              <span className="flex flex-shrink md:w-1/6 w-1/4 py-2 border-2 border-cyan-400 bg-cyan-900 rounded-bl-xl rounded-tr-xl mb-3 text-white text-center md:text-base text-sm items-center justify-center">
                Fantasy
              </span>
              <span className="text-left text-white text-2xl md:block hidden">
                &#9733;&#9733;&#9733;&#9733;&#9733;
              </span>
              <h3 className="md:text-4xl text-2xl font-bold text-left text-white mb-3">
                {item?.title}
              </h3>
              <p className="text-white text-left md:block hidden mb-5">
                {item?.overview}
              </p>
              <button
                onClick={_onClick}
                className="flex px-4 py-3 border-2 border-yellow-400 rounded-3xl text-white font-bold">
                Watch Now
              </button>
            </div>
            <div className="flex-initial" />
          </div>

          <figure className="object-cover md:h-screen h-72 max-w-screen-md md:max-w-full relative">
            <Image
              src={`${IMG_URL}${item?.backdrop_path}`}
              alt="Poster Movie"
              layout="fill"
              loading="lazy"
              objectFit="cover"
            />
          </figure>
        </div>
      );
    });
  }, [data, onClick]);

  return (
    <section>
      <Carousel showThumbs={false} autoPlay infiniteLoop showStatus={false}>
        {renderList}
      </Carousel>
    </section>
  );
}

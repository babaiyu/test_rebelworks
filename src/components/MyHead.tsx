import {useMemo} from 'react';
import Head from 'next/head';

const defaultTitle = 'Test Rebelworks';

interface Props {
  title?: string;
}

export default function MyHead({title = defaultTitle}: Props) {
  const theTitle = useMemo(() => {
    if (title) return `${defaultTitle} - ${title}`;
    return title;
  }, [title]);

  return (
    <Head>
      <title>{theTitle}</title>
    </Head>
  );
}

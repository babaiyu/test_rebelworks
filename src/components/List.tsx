import Image from 'next/image';
import {useCallback, useMemo} from 'react';

const IMG_URL = process.env.IMG_URL || '';

interface Props {
  title: string;
  data: any[];
  onClick: (id: any) => void;
}

export default function List({title, data, onClick}: Props) {
  const _onClick = useCallback(
    (id: any) => {
      onClick(id);
    },
    [onClick],
  );

  const renderList = useMemo(() => {
    return data.map(item => {
      return (
        <div key={item?.id} className="md:w-64 w-56 md:h-72 h-64 px-10">
          <figure className="object-cover md:w-64 w-56 md:h-72 h-64 relative overflow-hidden -z-10 top-0">
            <Image
              src={`${IMG_URL}${item?.poster_path}`}
              alt="Poster Movie"
              layout="fill"
              loading="lazy"
              objectFit="cover"
            />
          </figure>

          <div
            onClick={() => _onClick(item?.id)}
            className="z-10 md:-mt-72 -mt-64 h-full w-full flex flex-col items-start justify-end py-5 px-5 bg-gradient-to-r from-gray-900 cursor-pointer">
            <span className="text-cyan-300 text-sm bg-cyan-900 to-transparent px-3 py-2 rounded-tr-xl rounded-bl-xl mb-3">
              Fantasi
            </span>
            <span className="text-left text-white md:text-xl text-lg">
              &#9733;&#9733;&#9733;&#9733;&#9733;
            </span>
            <p className="text-white font-bold md:text-xl text-lg">
              {item?.title}
            </p>
          </div>
        </div>
      );
    });
  }, [data, _onClick]);

  return (
    <section className="my-5">
      <div className="flex justify-between px-10">
        <h2 className="md:text-2xl text-xl font-semibold text-white">
          {title}
        </h2>
        <button className="text-yellow-500">See All {' >'}</button>
      </div>

      <div className="flex overflow-x-auto space-x-8 w-full md:h-80 h-72 mt-5">
        {renderList}
      </div>
    </section>
  );
}

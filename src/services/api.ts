import axios from 'axios';
const URL = process.env.URL;
const API_KEY = process.env.API_KEY;

export const apiGetMovies = async (page: number) => {
  const url = `${URL}/movie/now_playing?api_key=${API_KEY}&language=en-US&page=${page}`;
  return await axios.get(url);
};

export const apiMovieID = async (id: number) => {
  const url = `${URL}/movie/${id}?api_key=${API_KEY}&language=en-US`;
  return await axios.get(url);
};

export const apiSimilar = async (id: number) => {
  const url = `${URL}/movie/${id}/similar?api_key=${API_KEY}&language=en-US&page=1`;
  return await axios.get(url);
};

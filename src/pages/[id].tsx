import Image from 'next/image';
import {useRouter} from 'next/router';
import {useEffect, useState} from 'react';
import {useQuery} from 'react-query';
import {List, MyHead} from '../components';
import {apiMovieID, apiSimilar} from '../services/api';

const IMG_URL = process.env.IMG_URL || '';

export default function Detail() {
  const [id, setId] = useState<number | any>(0);
  const router = useRouter();

  const _onBack = () => {
    requestAnimationFrame(() => {
      router.push('/');
    });
  };

  const movie = useQuery(
    ['movie', id],
    async () => await apiMovieID(id).then(res => res.data),
  );
  const movieSimilar = useQuery(
    ['similar', id],
    async () => await apiSimilar(id).then(res => res.data),
  );

  useEffect(() => {
    const onChangeId = () => {
      setId(router.query?.id);
    };

    onChangeId();
  }, [router.query.id]);

  const detail = movie?.data;
  const similar = movieSimilar?.data;

  const _onNavigate = (id: any) => {
    requestAnimationFrame(() => {
      console.log('ID navigate => ', id);
      router.push(`/${id}`);
    });
  };

  if (movie.isLoading) return 'Loading...';

  if (movie.error)
    return 'An error has occurred: ' + JSON.stringify(movie.error);

  return (
    <>
      <MyHead title={detail?.title} />
      <nav className="p-10 md:relative absolute">
        <figure
          onClick={_onBack}
          className="object-cover md:h-10 md:w-10 h-7 w-7 relative mb-3 flex-initial cursor-pointer">
          <Image
            src="/assets/logo.png"
            alt="Icon Logo"
            loading="lazy"
            layout="fill"
          />
        </figure>
      </nav>

      <section className="md:flex md:space-x-10">
        <div className="flex-initial">
          <div className="md:w-80 w-screen md:h-96 h-80 md:px-10">
            <figure className="object-cover md:w-80 w-screen md:h-96 h-80 relative overflow-hidden -z-10 top-0">
              <Image
                src={`${IMG_URL}${detail?.poster_path}`}
                alt="Poster Movie"
                layout="fill"
                loading="lazy"
                objectFit="cover"
              />
            </figure>

            <div className="z-10 md:-mt-96 -mt-80 h-full w-full flex flex-col items-start justify-end p-10 bg-gradient-to-r from-gray-900">
              <span className="text-cyan-300 text-sm bg-cyan-900 to-transparent px-3 py-2 rounded-tr-xl rounded-bl-xl mb-3">
                {detail?.genres[0]?.name}
              </span>
              <p>
                <span className="text-left text-white md:text-xl text-lg">
                  &#9733;&#9733;&#9733;&#9733;&#9733;
                </span>
                <span className="text-sm text-gray-200 ml-3">
                  &#8226; {detail?.release_date.split('-')[0]}
                </span>
              </p>
              <p className="text-white font-bold md:text-xl text-lg">
                {detail?.title}
              </p>
            </div>
          </div>
        </div>

        <div className="flex-auto p-10">
          <div className="md:flex md:space-x-8">
            <div className="flex-auto">
              <h1 className="text-2xl text-white font-bold mb-3">Synopsis</h1>
              <div className="p-3 bg-gray-700 text-gray-400">
                {detail?.overview}
              </div>
            </div>
            <div className="flex-initial md:mt-0 mt-5 w-96">
              <span className="text-lg text-gray-400">Cast</span>
              <p className="text-white">Mamank</p>
            </div>
          </div>

          <div className="mt-5 py-3 border-b-2 border-gray-500">
            <h1 className="text-2xl text-white font-bold mb-3">Episodes</h1>
            <div className="flex space-x-4">
              <div className="flex-auto">
                <figure className="object-cover md:w-60 w-32 md:h-40 h-24 relative">
                  <Image
                    src={`${IMG_URL}${detail?.backdrop_path}`}
                    alt="Backdrop Movie"
                    layout="fill"
                    loading="lazy"
                    objectFit="cover"
                  />
                </figure>
              </div>
              <div className="flex-auto">
                <div className="md:flex justify-between mb-3">
                  <h2 className="text-lg font-bold text-white">
                    1 - {detail?.title}
                  </h2>
                  <span className="text-gray-400">{detail?.runtime}m</span>
                </div>
                <div className="mx:max-h-40 h-24 text-ellipsis overflow-hidden">
                  <p className="text-base text-gray-400">{detail?.overview}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="my-10">
        <List
          title="You Might Also Like This!"
          data={similar?.results || []}
          onClick={_onNavigate}
        />
      </section>
    </>
  );
}

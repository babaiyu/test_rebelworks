import {useRouter} from 'next/router';
import {useState} from 'react';
import {useQuery} from 'react-query';
import {List, MyCarousel, MyHead} from '../components';
import {apiGetMovies} from '../services/api';

export default function Home() {
  const [page, setPage] = useState(1);
  const [movies, setMovies] = useState<any[]>([]);

  const router = useRouter();
  const {isLoading, error} = useQuery(
    ['movies', page],
    async () =>
      await apiGetMovies(page).then(res => {
        setMovies(res.data?.results);
        return res.data;
      }),
  );

  const _onNavigate = (id: any) => {
    requestAnimationFrame(() => {
      console.log('ID navigate => ', id);
      router.push(`/${id}`);
    });
  };

  if (isLoading) return 'Loading...';

  if (error) return 'An error has occurred: ' + JSON.stringify(error);

  return (
    <>
      <MyHead />
      <MyCarousel data={[...movies.slice(0, 5)]} onClick={_onNavigate} />
      <List title="New Release" data={movies} onClick={_onNavigate} />
      <List title="TV Shows" data={movies} onClick={_onNavigate} />
      <List title="Popular" data={movies} onClick={_onNavigate} />
    </>
  );
}
